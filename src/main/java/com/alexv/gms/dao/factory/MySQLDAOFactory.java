package com.alexv.gms.dao.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.impl.member.MemberDAOMySQLImpl;
import com.alexv.gms.dao.impl.membership.MembershipDAOMySQLImpl;

public class MySQLDAOFactory implements DAOFactory {
	
	private static final Logger log = Logger.getLogger(MySQLDAOFactory.class);
	
	public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost:3306/gms";
    public static final String DB_USER = "root";
    public static final String DB_PASSWORD = "mysql";
    
    public static Connection getConnection() throws SQLException {
    	try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			log.error(e);
		}
    	return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    }

    @Override
	public MemberDAO getMemberDao() {
		return MemberDAOMySQLImpl.getInstance();
	}

	@Override
	public MembershipDAO getMembershipDao() {
		return MembershipDAOMySQLImpl.getInstance();
	}
    
}
