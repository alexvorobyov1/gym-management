package com.alexv.gms.dao;

import java.util.List;

import com.alexv.gms.domain.Member;

public interface MemberDAO {
	
	Member getById(Long id);
	
	void add(Member member);
	
	void update(Member member);
	
	void deleteById(Long id);
	
	List<Member> getAll();
	
}
