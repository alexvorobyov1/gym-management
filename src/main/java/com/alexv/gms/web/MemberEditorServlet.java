package com.alexv.gms.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.factory.XMLDAOFactory;
import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.domain.Member;

@WebServlet("/member")
public class MemberEditorServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private DAOFactory daoFactory = new XMLDAOFactory();
	private MemberDAO memberDao = daoFactory.getMemberDao();
	private MembershipDAO membershipDao = daoFactory.getMembershipDao();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");	
		if (id != null && !id.isEmpty()) {
			Member member = memberDao.getById(Long.valueOf(id));
			request.setAttribute("member", member);
		}
		request.setAttribute("memberships", membershipDao.getAll());

		getServletContext().getRequestDispatcher("/WEB-INF/pages/member-form.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Member member = new Member();
		String id = request.getParameter("id");
		member.setFirstName(request.getParameter("firstName"));
		member.setLastName(request.getParameter("lastName"));
		if (!request.getParameter("membershipId").isEmpty()) {
			member.setMembership(membershipDao.getById(Long.valueOf(request.getParameter("membershipId"))));
		} else {
			member.setMembership(null);
		}
		if (id == null || id.isEmpty()) {
			memberDao.add(member);
		} else {
			member.setId(Long.valueOf(id));
			memberDao.update(member);
		}
		
		response.sendRedirect(request.getContextPath() + "/members");
	}
	
}
