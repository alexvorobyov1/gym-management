package com.alexv.gms.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.alexv.gms.dto.list.EndTimeDTO;
import com.alexv.gms.dto.list.MembersIdDTO;
import com.alexv.gms.dto.list.StartTimeDTO;

@XmlRootElement(name = "membership")
@XmlAccessorType(XmlAccessType.FIELD)
public class MembershipDTO {

	private Long id;
	private String name;
	private Double price;
	private StartTimeDTO startTime;
	private EndTimeDTO endTime;
	private MembersIdDTO membersId;
	
	public MembershipDTO() {}

	public MembershipDTO(Long id, String name, Double price, StartTimeDTO startTime,
						EndTimeDTO endTime, MembersIdDTO membersId) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.startTime = startTime;
		this.endTime = endTime;
		this.membersId = membersId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public StartTimeDTO getStartTime() {
		return startTime;
	}

	public void setStartTime(StartTimeDTO startTime) {
		this.startTime = startTime;
	}

	public EndTimeDTO getEndTime() {
		return endTime;
	}

	public void setEndTime(EndTimeDTO endTime) {
		this.endTime = endTime;
	}

	public MembersIdDTO getMembersId() {
		return membersId;
	}

	public void setMembersId(MembersIdDTO membersId) {
		this.membersId = membersId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((membersId == null) ? 0 : membersId.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		MembershipDTO other = (MembershipDTO) obj;
		return this.id != null && this.id.equals(other.id) &&
				this.name.equals(other.name) &&
				this.price.equals(other.price) &&
				this.startTime.equals(other.startTime) &&
				this.endTime.equals(other.endTime) &&
				this.membersId.equals(other.membersId);
	}

	@Override
	public String toString() {
		return "Membership [ID=" + id + ", Name=" + name + ", Price="
				+ price + ", Start time=" + startTime + ", End time="
				+ endTime + ", MembersId=" + membersId + "]";
	}
	
}
