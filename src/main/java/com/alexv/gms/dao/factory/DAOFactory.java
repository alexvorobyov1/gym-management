package com.alexv.gms.dao.factory;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;

public interface DAOFactory {
	
	public abstract MemberDAO getMemberDao();
	
	public abstract MembershipDAO getMembershipDao();
	
}
