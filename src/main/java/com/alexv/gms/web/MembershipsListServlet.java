package com.alexv.gms.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.factory.XMLDAOFactory;

@WebServlet("/memberships")
public class MembershipsListServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private DAOFactory daoFactory = new XMLDAOFactory();
	private MembershipDAO membershipDao = daoFactory.getMembershipDao();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("memberships", membershipDao.getAll());

		getServletContext().getRequestDispatcher("/WEB-INF/pages/memberships-list.jsp").forward(request, response);
	}
	
}
