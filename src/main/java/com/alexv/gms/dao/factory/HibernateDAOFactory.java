package com.alexv.gms.dao.factory;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.impl.member.MemberDAOHibernateImpl;
import com.alexv.gms.dao.impl.membership.MembershipDAOHibernateImpl;

public class HibernateDAOFactory implements DAOFactory {

	@Override
	public MemberDAO getMemberDao() {
		return MemberDAOHibernateImpl.getInstance();
	}

	@Override
	public MembershipDAO getMembershipDao() {
		return MembershipDAOHibernateImpl.getInstance();
	}

}
