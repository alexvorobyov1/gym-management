package com.alexv.gms.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.alexv.gms.dto.MembershipDTO;

@XmlRootElement(name = "memberships")
@XmlAccessorType(XmlAccessType.FIELD)
public class MembershipsDTO {
	
	@XmlElement(name = "membership")
    private List<MembershipDTO> memberships;
	
	public MembershipsDTO() {
		memberships = new ArrayList<MembershipDTO>();
	}
 
    public List<MembershipDTO> getMemberships() {
        return memberships;
    }
 
    public void setMemberships(List<MembershipDTO> memberships) {
        this.memberships = memberships;
    }
	
}
