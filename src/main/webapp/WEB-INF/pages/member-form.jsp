<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Member Form</title>
</head>

<body>

	<h1>Member Form</h1>

	<form method="post" action="${pageContext.request.contextPath}/member">
		<fieldset>
			<legend>
				<c:choose>
					<c:when test="${member.id != null && member.id != 0}">
						Updating a member
					</c:when>
					<c:otherwise>
						Adding a member
					</c:otherwise>
				</c:choose>
			</legend>

			<div>
				<label for="firstName">First Name</label> <input type="text" name="firstName"
					id="firstName" value="${member.firstName}" />
			</div>

			<div>
				<label for="lastName">Last Name</label> <input type="text" name="lastName"
					id="lastName" value="${member.lastName}" />
			</div>

			<div>
				<label for="membership">Membership</label>
				<select name="membershipId" id="membershipId">
					<option value="" ${member.membership == null ? 'selected' : ''}>Non-member</option>
					<c:forEach var="membership" items="${memberships}">
						<option value="${membership.id}" ${member.membership.id == membership.id ? 'selected' : ''}>${membership.name}</option>
					</c:forEach>
				</select>
			</div>

			<c:if test="${member.id != null && member.id != 0}">
				<input type="hidden" name="id" value="${member.id}" />
				<input type="hidden" name="registrationDate" value="${member.registrationDate}" />
			</c:if>
		</fieldset>

		<div class="button-row">
			<a href="${pageContext.request.contextPath}/members">Cancel</a> or <input type="submit" value="Submit" />
		</div>
	</form>

</body>
</html>