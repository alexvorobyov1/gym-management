package com.alexv.gms.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.factory.XMLDAOFactory;
import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;

@WebServlet("/members")
public class MembersListServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private DAOFactory daoFactory = new XMLDAOFactory();
	private MemberDAO memberDao = daoFactory.getMemberDao();
	private MembershipDAO membershipDao = daoFactory.getMembershipDao();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (request.getParameter("membershipId") == null) {
			request.setAttribute("members", memberDao.getAll());
		} else {
			request.setAttribute("members", membershipDao.getById(Long.valueOf(request.getParameter("membershipId")))
					.getMembers());
		}
		
		getServletContext().getRequestDispatcher("/WEB-INF/pages/members-list.jsp").forward(request, response);
	}
	
}
