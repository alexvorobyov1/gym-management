package com.alexv.gms.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.alexv.gms.dto.MemberDTO;

@XmlRootElement(name = "members")
@XmlAccessorType(XmlAccessType.FIELD)
public class MembersDTO {
	
	@XmlElement(name = "member")
    private List<MemberDTO> members;
	
	public MembersDTO() {
		members = new ArrayList<MemberDTO>();
	}
 
    public List<MemberDTO> getMembers() {
        return members;
    }
 
    public void setMembers(List<MemberDTO> members) {
        this.members = members;
    }
	
}
