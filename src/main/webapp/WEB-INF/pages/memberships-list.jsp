<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Memberships List</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/membership">Add new membership</a>

<table border="1" bordercolor="black">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Price</th>
		<th>Start Time</th>
		<th>End Time</th>
	</tr>
	
	<c:forEach var="membership" items="${memberships}">
		<tr>
			<td>${membership.id}</td>
			<td>${membership.name}</td>
			<td><fmt:setLocale value="en_US"/><fmt:formatNumber value="${membership.price}" type="currency" /></td>
			<td><c:forEach var="time" items="${membership.startTime}">
				<fmt:formatDate value="${time}" pattern="hh:mm a" /><br />
				</c:forEach>
			</td>
			<td><c:forEach var="time" items="${membership.endTime}">
				<fmt:formatDate value="${time}" pattern="hh:mm a" /><br />
				</c:forEach>
			</td>
			<td bordercolor="white">
			<form method="get" action="${pageContext.request.contextPath}/membership">
				<input type="hidden" name="id" value="${membership.id}">
				<input type="submit" value="Edit" />
			</form>
			</td>
			<td bordercolor="white">
			<form method="post" action="${pageContext.request.contextPath}/delete-membership">
				<input type="hidden" name="id" value="${membership.id}">
				<input type="submit" value="Delete" />
			</form>
			</td>
		</tr>
	</c:forEach>
</table>

<a href="${pageContext.request.contextPath}">Home</a>

</body>
</html>