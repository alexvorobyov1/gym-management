<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Membership Form</title>
</head>

<body>

	<h1>Membership Form</h1>
	<table>
		<tr>
			<td>
				Change number of times
			</td>
			<td>
				<form method="get" action="${pageContext.request.contextPath}/membership">
				<input type="hidden" name="id" value="${membership.id}">
				<input type="hidden" name="timeListSize" value="${timeListSize >= 1 ? timeListSize+1 : 1}">
				<input type="submit" value="+" />
				</form>
			</td>
			<td>
				<form method="get" action="${pageContext.request.contextPath}/membership">
				<input type="hidden" name="id" value="${membership.id}">
				<input type="hidden" name="timeListSize" value="${timeListSize > 1 ? timeListSize-1 : 1}">
				<input type="submit" value="-" />
				</form>
			</td>
		</tr>
	</table>
	<form method="post" action="${pageContext.request.contextPath}/membership">
		<fieldset>
			<legend>
				<c:choose>
					<c:when test="${membership.id != null && membership.id != 0}">
						Updating a membership
					</c:when>
					<c:otherwise>
						Adding a membership
					</c:otherwise>
				</c:choose>
			</legend>

			<div>
				<label for="name">Name</label> <input type="text" name="name"
					id="name" value="${membership.name}" />
			</div>

			<div>
				<fmt:setLocale value="en_US"/>
				<label for="price">Price $</label> <input type="text" name="price"
					id="price" value="<fmt:formatNumber value="${membership.price}" minFractionDigits="2" maxFractionDigits="2"/>" />
			</div>

			<div>
				<label for="time">Time</label>
			<table>
			<tr>
				<td><c:forEach var="i" begin="0" end="${timeListSize-1}">
				<input type="text" name="startTime${i}" id="startTime${i}" value="<fmt:formatDate value="${membership.startTime[i]}" pattern="hh:mm a" />" /><br />
				</c:forEach></td>
				<td><c:forEach var="i" begin="0" end="${timeListSize-1}">-<br /></c:forEach></td>
				<td><c:forEach var="i" begin="0" end="${timeListSize-1}">
				<input type="text" name="endTime${i}" id="endTime${i}" value="<fmt:formatDate value="${membership.endTime[i]}" pattern="hh:mm a" />" /><br />
				</c:forEach></td>
			</tr>
			</table>
			</div>

			<c:if test="${membership.id != null && membership.id != 0}">
				<input type="hidden" name="id" value="${membership.id}" />
			</c:if>
			<input type="hidden" name="timeListSize" value="${timeListSize}" />
		</fieldset>

		<div class="button-row">
			<a href="${pageContext.request.contextPath}/memberships">Cancel</a> or <input type="submit" value="Submit" />
		</div>
	</form>

</body>
</html>