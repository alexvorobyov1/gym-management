package com.alexv.gms.dao.impl.member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.factory.MySQLDAOFactory;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;

public class MemberDAOMySQLImpl implements MemberDAO {
	
	private static final Logger log = Logger.getLogger(MemberDAOMySQLImpl.class);
	
	private static MemberDAOMySQLImpl instance;
	
	private MemberDAOMySQLImpl() {}
	
	public static MemberDAOMySQLImpl getInstance() {
		if (instance == null) {
			instance = new MemberDAOMySQLImpl();
		}
		return instance;
	}
	
	@Override
	public Member getById(Long id) {
		Member member = null;
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMembers = conn.prepareStatement("SELECT * FROM members WHERE id = ?");
			PreparedStatement psMemberships = conn.prepareStatement("SELECT * FROM memberships WHERE id = ?");
			PreparedStatement psStartTime = conn.prepareStatement("SELECT * FROM start_time WHERE membership_id = ?");
			PreparedStatement psEndTime = conn.prepareStatement("SELECT * FROM end_time WHERE membership_id = ?")) {			
			psMembers.setLong(1, id);
			Long membershipId = null;
			try (ResultSet rsMembers = psMembers.executeQuery()) {
				while (rsMembers.next()) { 
					member = new Member(rsMembers.getLong("id"), rsMembers.getString("first_name"),
							rsMembers.getString("last_name"), null,
							rsMembers.getTimestamp("registration_date") == null ? null : 
							new Date(rsMembers.getTimestamp("registration_date").getTime()));
					membershipId = rsMembers.getLong("membership_id");
				}
			} catch (SQLException e) {
				log.error(e);
			}
		
			if (membershipId != null) {
				psStartTime.setLong(1, membershipId);
				psEndTime.setLong(1, membershipId);
				psMemberships.setLong(1, membershipId);
				Membership membership = null;
				
				List<Time> startTime = new ArrayList<Time>();
				try (ResultSet rsStartTime = psStartTime.executeQuery()) {
					while (rsStartTime.next()) { 
						startTime.add(rsStartTime.getTime("time"));
					}
				} catch (SQLException e) {
					log.error(e);
				}
				
				List<Time> endTime = new ArrayList<Time>();
				try (ResultSet rsEndTime = psEndTime.executeQuery()) {
					while (rsEndTime.next()) { 
						endTime.add(rsEndTime.getTime("time"));
					}
				} catch (SQLException e) {
					log.error(e);
				}
				
				try (ResultSet rsMemberships = psMemberships.executeQuery()) {
					while (rsMemberships.next()) { 
						membership = new Membership(rsMemberships.getLong("id"), rsMemberships.getString("name"),
								rsMemberships.getDouble("price"), startTime, endTime, null);
					}
				} catch (SQLException e) {
					log.error(e);
				}
				
				member.setMembership(membership);
			}
		} catch (SQLException e) {
			log.error(e);
		}
		return member;	
	}
	
	@Override
	public void add(Member member) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement ps = conn.prepareStatement("INSERT INTO members (first_name, last_name,"
					+ " registration_date, membership_id) VALUES (?, ?, ?, ?)")) {
			ps.setString(1, member.getFirstName());
			ps.setString(2, member.getLastName());
			ps.setTimestamp(3, new Timestamp(new Date().getTime()));
			if (member.getMembership() != null) {
				ps.setLong(4, member.getMembership().getId());
			} else {
				ps.setNull(4, Types.BIGINT);
			}
			ps.executeUpdate();		
		} catch (SQLException e) {
			log.error(e);
		}
	}

	@Override
	public void update(Member member) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE members SET first_name = ?, last_name = ?,"
					+ " membership_id = ? WHERE id = ?")) {
			ps.setString(1, member.getFirstName());
			ps.setString(2, member.getLastName());
			ps.setLong(3, member.getMembership().getId());
			ps.setLong(4, member.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			log.error(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM members WHERE id = ?")) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			log.error(e);
		}
	}

	@Override
	public List<Member> getAll() {
		List<Member> membersList = new ArrayList<Member>();
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMembers = conn.prepareStatement("SELECT * FROM members");
			PreparedStatement psMemberships = conn.prepareStatement("SELECT * FROM memberships WHERE id = ?");
			PreparedStatement psStartTime = conn.prepareStatement("SELECT * FROM start_time WHERE membership_id = ?");
			PreparedStatement psEndTime = conn.prepareStatement("SELECT * FROM end_time WHERE membership_id = ?");
			ResultSet rsMembers = psMembers.executeQuery();) { 
			while (rsMembers.next()) {
				Member member = new Member(rsMembers.getLong("id"), rsMembers.getString("first_name"),
						rsMembers.getString("last_name"), null,
						rsMembers.getTimestamp("registration_date") == null ? null :
						new Date(rsMembers.getTimestamp("registration_date").getTime()));
				
				if (rsMembers.getLong("membership_id") != 0) {
					psMemberships.setLong(1, rsMembers.getLong("membership_id"));
					psStartTime.setLong(1, rsMembers.getLong("membership_id"));
					psEndTime.setLong(1, rsMembers.getLong("membership_id"));
					Membership membership = null;
					
					List<Time> startTime = new ArrayList<Time>();
					try (ResultSet rsStartTime = psStartTime.executeQuery()) {
						while (rsStartTime.next()) { 
							startTime.add(rsStartTime.getTime("time"));
						}
					} catch (SQLException e) {
						log.error(e);
					}
					
					List<Time> endTime = new ArrayList<Time>();
					try (ResultSet rsEndTime = psEndTime.executeQuery()) {
						while (rsEndTime.next()) { 
							endTime.add(rsEndTime.getTime("time"));
						}
					} catch (SQLException e) {
						log.error(e);
					}
					
					try (ResultSet rsMemberships = psMemberships.executeQuery()) {
						while (rsMemberships.next()) { 
							membership = new Membership(rsMemberships.getLong("id"), rsMemberships.getString("name"),
									rsMemberships.getDouble("price"), startTime, endTime, null);
						}
					} catch (SQLException e) {
						log.error(e);
					}
					
					member.setMembership(membership);
				} 
				membersList.add(member);
			}
		} catch (SQLException e) {
			log.error(e);
		} 
		return membersList;
	}
	
}
