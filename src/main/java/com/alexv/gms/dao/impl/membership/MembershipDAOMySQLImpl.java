package com.alexv.gms.dao.impl.membership;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.factory.MySQLDAOFactory;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;
import com.mysql.jdbc.Statement;

public class MembershipDAOMySQLImpl implements MembershipDAO {
	
	private static final Logger log = Logger.getLogger(MembershipDAOMySQLImpl.class);
	
	private static MembershipDAOMySQLImpl instance;
	
	private MembershipDAOMySQLImpl() {}
	
	public static MembershipDAOMySQLImpl getInstance() {
		if (instance == null) {
			instance = new MembershipDAOMySQLImpl();
		}
		return instance;
	}

	@Override
	public Membership getById(Long id) {
		Membership membership = null;
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMemberships = conn.prepareStatement("SELECT * FROM memberships WHERE id = ?");
			PreparedStatement psMembers = conn.prepareStatement("SELECT * FROM members WHERE membership_id = ?");
			PreparedStatement psStartTime = conn.prepareStatement("SELECT * FROM start_time WHERE membership_id = ?");
			PreparedStatement psEndTime = conn.prepareStatement("SELECT * FROM end_time WHERE membership_id = ?")) {			
			psMemberships.setLong(1, id);
			psMembers.setLong(1, id);
			psStartTime.setLong(1, id);
			psEndTime.setLong(1, id);
			try (ResultSet rsMemberships = psMemberships.executeQuery()) {
				while (rsMemberships.next()) { 
					membership = new Membership(rsMemberships.getLong("id"), rsMemberships.getString("name"),
							rsMemberships.getDouble("price"), null, null, null);
				}
			} catch (SQLException e) {
				log.error(e);
			}
			List<Time> startTime = new ArrayList<Time>();
			try (ResultSet rsStartTime = psStartTime.executeQuery()) {
				while (rsStartTime.next()) { 
					startTime.add(rsStartTime.getTime("time"));
				}
			} catch (SQLException e) {
				log.error(e);
			}
			membership.setStartTime(startTime);
			List<Time> endTime = new ArrayList<Time>();
			try (ResultSet rsEndTime = psEndTime.executeQuery()) {
				while (rsEndTime.next()) { 
					endTime.add(rsEndTime.getTime("time"));
				}
			} catch (SQLException e) {
				log.error(e);
			}
			membership.setEndTime(endTime);
			List<Member> members = new ArrayList<Member>();
			try (ResultSet rsMembers = psMembers.executeQuery()) {
				while (rsMembers.next()) { 
					members.add(new Member(rsMembers.getLong("id"), rsMembers.getString("first_name"), rsMembers.getString("last_name"),
							membership, rsMembers.getTimestamp("registration_date") == null ? null : new Date(rsMembers.getTimestamp("registration_date").getTime())));
				}
			} catch (SQLException e) {
				log.error(e);
			}
			membership.setMembers(members);
		} catch (SQLException e) {
			log.error(e);
		}
		return membership;	
	}

	@Override
	public void add(Membership membership) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMemberships = conn.prepareStatement("INSERT INTO memberships (name, price)"
											+ " VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS);
			PreparedStatement psStartTime = conn.prepareStatement("INSERT INTO start_time (time, membership_id)"
											+ " VALUES (?, ?)");
			PreparedStatement psEndTime = conn.prepareStatement("INSERT INTO end_time (time, membership_id)"
											+ " VALUES (?, ?)")) {
			psMemberships.setString(1, membership.getName());
			psMemberships.setDouble(2, membership.getPrice());
			psMemberships.executeUpdate();
				
			ResultSet rsMemberships = psMemberships.getGeneratedKeys();
			rsMemberships.next();
			Long id = rsMemberships.getLong(1);
			
			for (Time time : membership.getStartTime()) {
				psStartTime.setTime(1, time);
				psStartTime.setLong(2, id);
				psStartTime.executeUpdate();
			}
			
			for (Time time : membership.getEndTime()) {
				psEndTime.setTime(1, time);
				psEndTime.setLong(2, id);
				psEndTime.executeUpdate();
			}
		} catch (SQLException e) {
			log.error(e);
		}
	}

	@Override
	public void update(Membership membership) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMemberships = conn.prepareStatement("UPDATE memberships SET name = ?,"
					+ " price = ? WHERE id = ?");
			PreparedStatement psDeleteStartTime = conn.prepareStatement("DELETE FROM start_time"
					+ " WHERE membership_id = ?");
			PreparedStatement psDeleteEndTime = conn.prepareStatement("DELETE FROM end_time"
					+ " WHERE membership_id = ?");
			PreparedStatement psStartTime = conn.prepareStatement("INSERT INTO start_time (time, membership_id)"
					+ " VALUES (?, ?)");
			PreparedStatement psEndTime = conn.prepareStatement("INSERT INTO end_time (time, membership_id)"
					+ " VALUES (?, ?)")) {
			psMemberships.setString(1, membership.getName());
			psMemberships.setDouble(2, membership.getPrice());
			psMemberships.setLong(3, membership.getId());
			psMemberships.executeUpdate();
			
			psDeleteStartTime.setLong(1, membership.getId());
			psDeleteStartTime.executeUpdate();
			
			psDeleteEndTime.setLong(1, membership.getId());
			psDeleteEndTime.executeUpdate();
			
			for (Time time : membership.getStartTime()) {
				psStartTime.setTime(1, time);
				psStartTime.setLong(2, membership.getId());
				psStartTime.executeUpdate();
			}
			
			for (Time time : membership.getEndTime()) {
				psEndTime.setTime(1, time);
				psEndTime.setLong(2, membership.getId());
				psEndTime.executeUpdate();
			}
		} catch (SQLException e) {
			log.error(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM memberships WHERE id = ?")) {
			ps.setLong(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			log.error(e);
		}	
	}

	@Override
	public List<Membership> getAll() {
		List<Membership> memberships = new ArrayList<Membership>();
		try (Connection conn = MySQLDAOFactory.getConnection();
			PreparedStatement psMemberships = conn.prepareStatement("SELECT * FROM memberships");
			PreparedStatement psMembers = conn.prepareStatement("SELECT * FROM members WHERE membership_id = ?");
			PreparedStatement psStartTime = conn.prepareStatement("SELECT * FROM start_time WHERE membership_id = ?");
			PreparedStatement psEndTime = conn.prepareStatement("SELECT * FROM end_time WHERE membership_id = ?")) {			
			try (ResultSet rsMemberships = psMemberships.executeQuery()) {
				while (rsMemberships.next()) {
					Membership membership = new Membership(rsMemberships.getLong("id"), rsMemberships.getString("name"),
							rsMemberships.getDouble("price"), null, null, null);
					
					psMembers.setLong(1, rsMemberships.getLong("id"));
					psStartTime.setLong(1, rsMemberships.getLong("id"));
					psEndTime.setLong(1, rsMemberships.getLong("id"));
					
					List<Time> startTime = new ArrayList<Time>();
					try (ResultSet rsStartTime = psStartTime.executeQuery()) {
						while (rsStartTime.next()) { 
							startTime.add(rsStartTime.getTime("time"));
						}
					} catch (SQLException e) {
						log.error(e);
					}
					membership.setStartTime(startTime);
					
					List<Time> endTime = new ArrayList<Time>();
					try (ResultSet rsEndTime = psEndTime.executeQuery()) {
						while (rsEndTime.next()) { 
							endTime.add(rsEndTime.getTime("time"));
						}
					} catch (SQLException e) {
						log.error(e);
					}
					membership.setEndTime(endTime);
					
					List<Member> members = new ArrayList<Member>();
					try (ResultSet rsMembers = psMembers.executeQuery()) {
						while (rsMembers.next()) { 
							members.add(new Member(rsMembers.getLong("id"), rsMembers.getString("first_name"), rsMembers.getString("last_name"),
									membership, rsMembers.getTimestamp("registration_date") == null ? null : new Date(rsMembers.getTimestamp("registration_date").getTime())));
						}
					} catch (SQLException e) {
						log.error(e);
					}
					membership.setMembers(members);
					
					memberships.add(membership);
				}
			} catch (SQLException e) {
				log.error(e);
			}
		} catch (SQLException e) {
			log.error(e);
		}
		return memberships;	
	}

}
