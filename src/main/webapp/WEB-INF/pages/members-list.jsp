<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Members List</title>
</head>
<body>

<a href="${pageContext.request.contextPath}/member">Add new member</a>

<table border="1" bordercolor="black">
	<tr>
		<th>ID</th>
		<th>First Name</th>
		<th>Last Name</th>
		<th>Membership</th>
		<th>Registration Date</th>
	</tr>
	
	<c:forEach var="member" items="${members}">
		<tr>
			<td>${member.id}</td>
			<td>${member.firstName}</td>
			<td>${member.lastName}</td>
			<td>
				<c:choose>
					<c:when test="${member.membership != null}">
						${member.membership.name}
					</c:when>
					<c:otherwise>
						Non-member
					</c:otherwise>
				</c:choose>
			</td>
			<td><fmt:formatDate value="${member.registrationDate}" pattern="yyyy-MM-dd hh:mm:ss a" /></td>
			<td bordercolor="white">
			<form method="get" action="${pageContext.request.contextPath}/member">
				<input type="hidden" name="id" value="${member.id}">
				<input type="submit" value="Edit" />
			</form>
			</td>
			<td bordercolor="white">
			<form method="post" action="${pageContext.request.contextPath}/delete-member">
				<input type="hidden" name="id" value="${member.id}">
				<input type="submit" value="Delete" />
			</form>
			</td>
		</tr>
	</c:forEach>
</table>

<a href="${pageContext.request.contextPath}">Home</a>

</body>
</html>