package com.alexv.gms.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "member")
@XmlAccessorType(XmlAccessType.FIELD)
public class MemberDTO {

	private Long id;
	private String firstName;
	private String lastName;
	private Long membershipId;
	private String registrationDate;
	
	public MemberDTO() {}
	
	public MemberDTO(Long id, String firstName, String lastName,
			Long membershipId, String registrationDate) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.membershipId = membershipId;
		this.registrationDate = registrationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getMembershipId() {
		return membershipId;
	}

	public void setMembershipId(Long membershipId) {
		this.membershipId = membershipId;
	}

	public String getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((membershipId == null) ? 0 : membershipId.hashCode());
		result = prime * result + ((registrationDate == null) ? 0 : registrationDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		MemberDTO other = (MemberDTO) obj;
		return this.id != null && this.id.equals(other.id) &&
				this.firstName.equals(other.firstName) &&
				this.lastName.equals(other.lastName) &&
				this.membershipId.equals(other.membershipId) &&
				this.registrationDate.equals(other.registrationDate);
	}

	@Override
	public String toString() {
		return "MemberDTO [id=" + id + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", membershipId=" + membershipId
				+ ", registrationDate=" + registrationDate + "]";
	}
	
}
