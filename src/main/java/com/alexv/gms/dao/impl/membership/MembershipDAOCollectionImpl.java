package com.alexv.gms.dao.impl.membership;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.domain.Membership;
import com.alexv.gms.domain.Member;

public class MembershipDAOCollectionImpl implements MembershipDAO {
	
	private static List<Membership> membershipsList = null;
	private static Long counter;
	private static MembershipDAOCollectionImpl instance;
	
	public static MembershipDAOCollectionImpl getInstance() {
		if (instance == null) {
			instance = new MembershipDAOCollectionImpl();
		}
		return instance;
	}
	
	private MembershipDAOCollectionImpl() {
		if (membershipsList == null) {
			membershipsList = new ArrayList<Membership>();
			membershipsList.add(new Membership(1L, "Bronze", 99.0, new ArrayList<Time>(
					Arrays.asList(Time.valueOf("06:30:00"))), new ArrayList<Time>(
					Arrays.asList(Time.valueOf("16:00:00"))), null));
			membershipsList.add(new Membership(2L, "Silver", 199.0, new ArrayList<Time>(
					Arrays.asList(Time.valueOf("06:30:00"), Time.valueOf("20:00:00"))), new ArrayList<Time>(
					Arrays.asList(Time.valueOf("16:00:00"), Time.valueOf("22:30:00"))), null));
			membershipsList.add(new Membership(3L, "Gold", 299.0, new ArrayList<Time>(
					Arrays.asList(Time.valueOf("06:30:00"))), new ArrayList<Time>(
					Arrays.asList(Time.valueOf("22:30:00"))), null));
			counter = 3L;
		}
	}

	@Override
	public Membership getById(Long id) {
		for (Membership membership : membershipsList) {
			if (membership.getId() == id) {
				return membership;
			}
		}
		return null;
	}

	@Override
	public void add(Membership membership) {
		++counter;
		membership.setId(counter);
		membership.setMembers(null);
		membershipsList.add(membership);
	}

	@Override
	public void update(Membership membership) {
		Membership updatedMembership = getById(membership.getId());
		updatedMembership.setName(membership.getName());
		updatedMembership.setPrice(membership.getPrice());
		updatedMembership.setStartTime(membership.getStartTime());
		updatedMembership.setEndTime(membership.getEndTime());
	}

	@Override
	public void deleteById(Long id) {
		Membership membership = getById(id);
		membershipsList.remove(membership);
		if (membership.getMembers() != null) {
			for (Member member : membership.getMembers()) {
				member.setMembership(null);
			}
		}
	}

	@Override
	public List<Membership> getAll() {
		return new ArrayList<Membership>(membershipsList);
	}

}
