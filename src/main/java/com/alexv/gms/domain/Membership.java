package com.alexv.gms.domain;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "memberships")
public class Membership implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "price")
	private Double price;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "start_time",
	joinColumns = @JoinColumn(name = "membership_id"))
	@Column(name = "time", nullable = true)
	private List<Time> startTime;
	
	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "end_time",
	joinColumns = @JoinColumn(name = "membership_id"))
	@Column(name = "time", nullable = true)
	private List<Time> endTime;
	
	@OneToMany(mappedBy = "membership")
	private List<Member> members;
	
	public Membership() {}

	public Membership(Long id, String name, Double price, List<Time> startTime,
						List<Time> endTime, List<Member> members) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.startTime = startTime;
		this.endTime = endTime;
		this.members = members;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public List<Time> getStartTime() {
		return startTime;
	}

	public void setStartTime(List<Time> startTime) {
		this.startTime = startTime;
	}

	public List<Time> getEndTime() {
		return endTime;
	}

	public void setEndTime(List<Time> endTime) {
		this.endTime = endTime;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endTime == null) ? 0 : endTime.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((members == null) ? 0 : members.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((startTime == null) ? 0 : startTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Membership other = (Membership) obj;
		return this.id != null && this.id.equals(other.id) &&
				this.name.equals(other.name) &&
				this.price.equals(other.price) &&
				this.startTime.equals(other.startTime) &&
				this.endTime.equals(other.endTime);
	}

	@Override
	public String toString() {
		return "Membership [ID=" + id + ", Name=" + name + ", Price="
				+ price + ", Start time=" + startTime + ", End time="
				+ endTime + "]";
	}
	
}
