package com.alexv.gms.dao.impl.membership;

import java.io.File;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.factory.XMLDAOFactory;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;
import com.alexv.gms.dto.MemberDTO;
import com.alexv.gms.dto.MembershipDTO;
import com.alexv.gms.dto.list.EndTimeDTO;
import com.alexv.gms.dto.list.MembersDTO;
import com.alexv.gms.dto.list.MembershipsDTO;
import com.alexv.gms.dto.list.StartTimeDTO;

public class MembershipDAOXMLImpl implements MembershipDAO {
	
	private static final Logger log = Logger.getLogger(MembershipDAOXMLImpl.class);

	private static MembershipDAOXMLImpl instance;
	private static final String membersFileName = "members.xml";
	private static final String membershipsFileName = "memberships.xml";
	private static File membersFile;
	private static File membershipsFile;
		
	private MembershipDAOXMLImpl() {
		membersFile = XMLDAOFactory.getXmlFile(membersFileName);
		membershipsFile = XMLDAOFactory.getXmlFile(membershipsFileName);
	}
		
	public static MembershipDAOXMLImpl getInstance() {
		if (instance == null) {
			instance = new MembershipDAOXMLImpl();
		}
		return instance;
	}

	@Override
	public Membership getById(Long id) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembershipsDTO membershipsDto = new MembershipsDTO();
			if (membershipsFile.length() != 0) {
				membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
			}
			
			for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
				if (membershipDto.getId() == id) {
					List<Time> startTime = new ArrayList<Time>();
					for (String time : membershipDto.getStartTime().getTimes()) {
						if (time != null) { 
							startTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
						}
					}
					List<Time> endTime = new ArrayList<Time>();
					for (String time : membershipDto.getEndTime().getTimes()) {
						if (time != null) { 
							endTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
						}
					}
					Membership membership = new Membership(membershipDto.getId(), membershipDto.getName(),
											membershipDto.getPrice(), startTime, endTime, null);
					
					if (membershipDto.getMembersId() != null) {
						jaxbContext = JAXBContext.newInstance(MembersDTO.class);
						jaxbUnmarshaller = jaxbContext.createUnmarshaller();
						MembersDTO membersDto = new MembersDTO();
						if (membersFile.length() != 0) {
							membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
						}	
						
						List<Member> members = new ArrayList<Member>();
						for (MemberDTO memberDto : membersDto.getMembers()) {
							if (membershipDto.getMembersId().getIds().contains(memberDto.getId())) {
								members.add(new Member(memberDto.getId(), memberDto.getFirstName(),
										memberDto.getLastName(), null,
										new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
										.parse(memberDto.getRegistrationDate())));
							}
						}
						membership.setMembers(members);
					}
					return membership;
				}
			}
		} catch (JAXBException e) {
			log.error(e);
		} catch (ParseException e) {
			log.error(e);
		}
		return null;
	}

	@Override
	public void add(Membership membership) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembershipsDTO membershipsDto = new MembershipsDTO();
			if (membershipsFile.length() != 0) {
				membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
			}

			Long id = 0L;
			if (membershipsDto.getMemberships().size() > 0) {
				id = membershipsDto.getMemberships().get(membershipsDto.getMemberships().size()-1).getId();
			}
			StartTimeDTO startTimeDto = new StartTimeDTO();
			for (Time time : membership.getStartTime()) {
				if (time != null) { 
					startTimeDto.getTimes().add(new SimpleDateFormat("hh:mm a").format(time));
				}
			}
			EndTimeDTO endTimeDto = new EndTimeDTO();
			for (Time time : membership.getEndTime()) {
				if (time != null) { 
					endTimeDto.getTimes().add(new SimpleDateFormat("hh:mm a").format(time));
				}
			}
			MembershipDTO newMembershipDTO = new MembershipDTO(++id, membership.getName(), membership.getPrice(),
											startTimeDto, endTimeDto, null);
			membershipsDto.getMemberships().add(newMembershipDTO);

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membershipsDto, membershipsFile);
		} catch (JAXBException e) {
			log.error(e);
		}
	}

	@Override
	public void update(Membership membership) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembershipsDTO membershipsDto = new MembershipsDTO();
			if (membershipsFile.length() != 0) {
				membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
			}
			
			for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
				if (membershipDto.getId() == membership.getId()) {
						membershipDto.setName(membership.getName());
						membershipDto.setPrice(membership.getPrice());
						List<String> startTime = new ArrayList<String>();
						for (Time time : membership.getStartTime()) {
							if (time != null) { 
								startTime.add(new SimpleDateFormat("hh:mm a").format(time));
							}
						}
						List<String> endTime = new ArrayList<String>();
						for (Time time : membership.getEndTime()) {
							if (time != null) { 
								endTime.add(new SimpleDateFormat("hh:mm a").format(time));
							}
						}
						StartTimeDTO startTimeDto = new StartTimeDTO();
						startTimeDto.setTimes(startTime);
						EndTimeDTO endTimeDto = new EndTimeDTO();
						endTimeDto.setTimes(endTime);
						membershipDto.setStartTime(startTimeDto);
						membershipDto.setEndTime(endTimeDto);
					break;
				}
			}
		
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membershipsDto, membershipsFile);
		} catch (JAXBException e) {
			log.error(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembershipsDTO membershipsDto = new MembershipsDTO();
			if (membershipsFile.length() != 0) {
				membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
			}
			
			List<Long> membersId = null;
			Iterator<MembershipDTO> membershipsDtoIt = membershipsDto.getMemberships().iterator();
			while (membershipsDtoIt.hasNext()) {
				MembershipDTO temp = membershipsDtoIt.next();
				if (temp.getId() == id) {
					if (temp.getMembersId() != null) {
						membersId = temp.getMembersId().getIds();
					}
					membershipsDtoIt.remove();
					break;
				}
			}

			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membershipsDto, membershipsFile);
			
			if (membersId != null) {
				jaxbContext = JAXBContext.newInstance(MembersDTO.class);
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				MembersDTO membersDto = new MembersDTO();
				if (membersFile.length() != 0) {
					membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
				}	
				
				for (MemberDTO memberDto : membersDto.getMembers()) {
					if (membersId.contains(memberDto.getId())) {
						memberDto.setMembershipId(null);
					}
				}
				
				jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				jaxbMarshaller.marshal(membersDto, membersFile);
			}
		} catch (JAXBException e) {
			log.error(e);
		}
	}

	@Override
	public List<Membership> getAll() {
		List<Membership> memberships = new ArrayList<Membership>();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembershipsDTO membershipsDto = new MembershipsDTO();
			if (membershipsFile.length() != 0) {
				membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
			}

			for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
				List<Time> startTime = new ArrayList<Time>();
				for (String time : membershipDto.getStartTime().getTimes()) {
					if (time != null) { 
						startTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
					}
				}
				List<Time> endTime = new ArrayList<Time>();
				for (String time : membershipDto.getEndTime().getTimes()) {
					if (time != null) { 
						endTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
					}
				}
				Membership membership = new Membership(membershipDto.getId(), membershipDto.getName(),
						membershipDto.getPrice(), startTime, endTime, null);
				
				if (membershipDto.getMembersId() != null) {
					jaxbContext = JAXBContext.newInstance(MembersDTO.class);
					jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					MembersDTO membersDto = new MembersDTO();
					if (membersFile.length() != 0) {
						membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
					}
					
					List<Member> members = new ArrayList<Member>();
					for (MemberDTO memberDto : membersDto.getMembers()) {
						if (membershipDto.getMembersId().getIds().contains(memberDto.getId())) {
							members.add(new Member(memberDto.getId(), memberDto.getFirstName(),
									memberDto.getLastName(), null,
									new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
									.parse(memberDto.getRegistrationDate())));
						}
					}
					membership.setMembers(members);
				}
				memberships.add(membership);
			}
		} catch (JAXBException e) {
			log.error(e);
		} catch (ParseException e) {
			log.error(e);
		}
		return memberships;	
	}

}
