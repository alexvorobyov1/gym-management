package com.alexv.gms.dao.impl.member;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;

public class MemberDAOCollectionImpl implements MemberDAO {
	
	private static List<Member> membersList = null;
	private static Long counter;
	private static MemberDAOCollectionImpl instance;
	
	private MemberDAOCollectionImpl() {
		if (membersList == null) {
			membersList = new ArrayList<Member>();
			counter = 0L;
		}
	}

	public static MemberDAOCollectionImpl getInstance() {
		if (instance == null) {
			instance = new MemberDAOCollectionImpl();
		}
		return instance;
	}
	
	@Override
	public Member getById(Long id) {
		for (Member member : membersList) {
			if (member.getId() == id) {
				return member;
			}
		}
		return null;
	}

	@Override
	public void add(Member member) {
		++counter;
		member.setId(counter);
		member.setRegistrationDate(new Date());
		membersList.add(member);
		
		Membership membership = member.getMembership();
		if (membership != null) {
			if (membership.getMembers() != null) {
				membership.getMembers().add(member);
			} else {
				membership.setMembers(new ArrayList<Member>(Arrays.asList(member)));
			}
		}
	}

	@Override
	public void update(Member member) {
		Member updatedMember = getById(member.getId());
		Membership oldMembership = updatedMember.getMembership();
		updatedMember.setFirstName(member.getFirstName());
		updatedMember.setLastName(member.getLastName());
		updatedMember.setMembership(member.getMembership());
		
		Membership newMembership = updatedMember.getMembership();
		if (oldMembership != null) {
			if (oldMembership.getMembers() != null) {
				oldMembership.getMembers().remove(updatedMember);
			}
		}
		if (newMembership != null) {
			if (newMembership.getMembers() != null) {
				newMembership.getMembers().add(updatedMember);
				Collections.sort(newMembership.getMembers(), new Comparator<Member>() {
					@Override
			        public int compare(Member member1, Member member2) {
			            return  member1.getId().compareTo(member2.getId());
			        }
			    });
			} else {
				newMembership.setMembers(new ArrayList<Member>(Arrays.asList(updatedMember)));
			}
		}
	}

	@Override
	public void deleteById(Long id) {
		Member member = getById(id);
		Membership membership = member.getMembership();
		membersList.remove(member);
		
		if (membership != null) {
			if (membership.getMembers() != null) {
				membership.getMembers().remove(member);
			} 
		}
	}

	@Override
	public List<Member> getAll() {
		return new ArrayList<Member>(membersList);
	}
	
}
