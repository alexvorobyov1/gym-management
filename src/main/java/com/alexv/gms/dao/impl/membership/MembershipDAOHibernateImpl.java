package com.alexv.gms.dao.impl.membership;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.domain.Membership;

public class MembershipDAOHibernateImpl implements MembershipDAO {

	private static MembershipDAOHibernateImpl instance;
	
	private static EntityManagerFactory emf;
	
	private MembershipDAOHibernateImpl() {}
	
	public static MembershipDAOHibernateImpl getInstance() {
		if (instance == null) {
			instance = new MembershipDAOHibernateImpl();
			emf = Persistence.createEntityManagerFactory("gms");
		}
		return instance;
	}

	@Override
	public Membership getById(Long id) {
		Membership membership = null;
		
	    EntityManager em = emf.createEntityManager();
	    em.getTransaction().begin();
	    
		membership = em.find(Membership.class, id);
		
		List<Time> startTime = new ArrayList<Time>();
		Query getStartTime = em.createNativeQuery("SELECT time FROM start_time WHERE membership_id = :id");
		getStartTime.setParameter("id", id);
		startTime = getStartTime.getResultList();
		
		List<Time> endTime = new ArrayList<Time>();
		Query getEndTime = em.createNativeQuery("SELECT time FROM end_time WHERE membership_id = :id");
		getEndTime.setParameter("id", id);
		endTime = getEndTime.getResultList();
		
		membership.setStartTime(startTime);
		membership.setEndTime(endTime);
		
		em.getTransaction().commit();
		em.close();
		
		return membership;
	}

	@Override
	public void add(Membership membership) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.persist(membership);
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void update(Membership membership) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.merge(membership);
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void deleteById(Long id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.remove(em.find(Membership.class, id));
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public List<Membership> getAll() {
		List<Membership> memberships = new ArrayList<Membership>();
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		memberships = em.createQuery("from Membership", Membership.class).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return memberships;
	}

}
