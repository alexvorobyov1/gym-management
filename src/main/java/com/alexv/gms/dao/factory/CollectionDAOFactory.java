package com.alexv.gms.dao.factory;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.impl.member.MemberDAOCollectionImpl;
import com.alexv.gms.dao.impl.membership.MembershipDAOCollectionImpl;

public class CollectionDAOFactory implements DAOFactory {
	
	@Override
	public MemberDAO getMemberDao() {
		return MemberDAOCollectionImpl.getInstance();
	}

	@Override
	public MembershipDAO getMembershipDao() {
		return MembershipDAOCollectionImpl.getInstance();
	}
	
}
