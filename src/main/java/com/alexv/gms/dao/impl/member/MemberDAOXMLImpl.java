package com.alexv.gms.dao.impl.member;

import java.io.File;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.factory.XMLDAOFactory;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;
import com.alexv.gms.dto.MemberDTO;
import com.alexv.gms.dto.MembershipDTO;
import com.alexv.gms.dto.list.MembersDTO;
import com.alexv.gms.dto.list.MembersIdDTO;
import com.alexv.gms.dto.list.MembershipsDTO;

public class MemberDAOXMLImpl implements MemberDAO {
	
	private static final Logger log = Logger.getLogger(MemberDAOXMLImpl.class);

	private static MemberDAOXMLImpl instance;
	private static final String membersFileName = "members.xml";
	private static final String membershipsFileName = "memberships.xml";
	private static File membersFile;
	private static File membershipsFile;
	
	private MemberDAOXMLImpl() {
		membersFile = XMLDAOFactory.getXmlFile(membersFileName);
		membershipsFile = XMLDAOFactory.getXmlFile(membershipsFileName);
	}
	
	public static MemberDAOXMLImpl getInstance() {
		if (instance == null) {
			instance = new MemberDAOXMLImpl();
		}
		return instance;
	}

	@Override
	public Member getById(Long id) {
		Member member = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembersDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembersDTO membersDto = new MembersDTO();
			if (membersFile.length() != 0) {
				membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
			}
			
			for (MemberDTO memberDto : membersDto.getMembers()) {
				if (memberDto.getId() == id) {
					member = new Member(memberDto.getId(), memberDto.getFirstName(), memberDto.getLastName(),
							null, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").parse(memberDto.getRegistrationDate()));
					
					if (memberDto.getMembershipId() != null) {
						jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
						jaxbUnmarshaller = jaxbContext.createUnmarshaller();
						MembershipsDTO membershipsDto = new MembershipsDTO();
						if (membershipsFile.length() != 0) {
							membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
						}
						
						for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
							if (membershipDto.getId() == memberDto.getMembershipId()) {
								List<Time> startTime = new ArrayList<Time>();
								for (String time : membershipDto.getStartTime().getTimes()) {
									if (time != null) { 
										startTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
									}
								}
								List<Time> endTime = new ArrayList<Time>();
								for (String time : membershipDto.getEndTime().getTimes()) {
									if (time != null) { 
										endTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
									}
								}
								member.setMembership(new Membership(membershipDto.getId(), membershipDto.getName(),
										membershipDto.getPrice(), startTime, endTime, null));
								break;
							}
						}
					}
					break;
				}
			}
		} catch (JAXBException e) {
			log.error(e);
		} catch (ParseException e) {
			log.error(e);
		}
		return member;
	}

	@Override
	public void add(Member member) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembersDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembersDTO membersDto = new MembersDTO();
			if (membersFile.length() != 0) {
				membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
			}
			
			Long id = 0L;
			if (membersDto.getMembers().size() > 0) {
				id = membersDto.getMembers().get(membersDto.getMembers().size()-1).getId();
			}
			MemberDTO newMemberDTO = new MemberDTO(++id, member.getFirstName(), member.getLastName(),
						member.getMembership() == null ? null : member.getMembership().getId(),
						new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a").format(new Date()));
			membersDto.getMembers().add(newMemberDTO);
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membersDto, membersFile);
			
			if (member.getMembership() != null) { 
				jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				MembershipsDTO membershipsDto = new MembershipsDTO();
				if (membershipsFile.length() != 0) {
					membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
				}
			
				for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
					if (membershipDto.getId() == member.getMembership().getId()) {
						if (membershipDto.getMembersId() != null) {
							membershipDto.getMembersId().getIds().add(id);
						} else {
							MembersIdDTO membersId = new MembersIdDTO();
							membersId.setIds(Arrays.asList(id));
							membershipDto.setMembersId(membersId);
						}
						break;
					}
				}
			
				jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				jaxbMarshaller.marshal(membershipsDto, membershipsFile);
			}
		} catch (JAXBException e) {
			log.error(e);
		}
	}

	@Override
	public void update(Member member) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembersDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembersDTO membersDto = new MembersDTO();
			if (membersFile.length() != 0) {
				membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
			}
			
			Long oldMembershipId = null;
			Long newMembershipId = null;
			for (MemberDTO memberDto : membersDto.getMembers()) {
				if (memberDto.getId() == member.getId()) {
					oldMembershipId = memberDto.getMembershipId();
					memberDto.setFirstName(member.getFirstName());
					memberDto.setLastName(member.getLastName());
					if (member.getMembership() != null) {
						memberDto.setMembershipId(member.getMembership().getId());
						newMembershipId = memberDto.getMembershipId();
					} else {
						memberDto.setMembershipId(null);
					}
					break;
				}
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membersDto, membersFile);
			
			if (oldMembershipId != newMembershipId) {
				jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				MembershipsDTO membershipsDto = new MembershipsDTO();
				if (membershipsFile.length() != 0) {
					membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
				}
				
				if (oldMembershipId != null) {
					for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
						if (membershipDto.getId() == oldMembershipId) {
							System.out.println(membershipDto.getMembersId());
							membershipDto.getMembersId().getIds().remove(member.getId());
							break;
						}
					}
				}
				
				if (newMembershipId != null) {
					for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
						if (membershipDto.getId() == newMembershipId) {
							if (membershipDto.getMembersId() != null) {
								membershipDto.getMembersId().getIds().add(member.getId());
							} else {
								MembersIdDTO membersId = new MembersIdDTO();
								membersId.setIds(Arrays.asList(member.getId()));
								membershipDto.setMembersId(membersId);
							}
							Collections.sort(membershipDto.getMembersId().getIds());
							break;
						}
					}
				}
				jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				jaxbMarshaller.marshal(membershipsDto, membershipsFile);
			}
		} catch (JAXBException e) {
			log.error(e);
		}
	}

	@Override
	public void deleteById(Long id) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembersDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembersDTO membersDto = new MembersDTO();
			if (membersFile.length() != 0) {
				membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
			}
			
			Long membershipId = null;
			Iterator<MemberDTO> membersDtoIt = membersDto.getMembers().iterator();
			while(membersDtoIt.hasNext()) {
				MemberDTO temp = membersDtoIt.next();
				if (temp.getId() == id) {
					membershipId = temp.getMembershipId();
					membersDtoIt.remove();
					break;
				}
			}
			
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(membersDto, membersFile);
			
			if (membershipId != null) {
				jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
				jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				MembershipsDTO membershipsDto = new MembershipsDTO();
				if (membershipsFile.length() != 0) {
					membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
				}
				
				for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
					if (membershipDto.getId() == membershipId) {
						membershipDto.getMembersId().getIds().remove(id);
						break;
					}
				}
				
				jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				jaxbMarshaller.marshal(membershipsDto, membershipsFile);
			}
		} catch (JAXBException e) {
			log.error(e);
		}
		
	}

	@Override
	public List<Member> getAll() {
		List<Member> members = new ArrayList<Member>();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(MembersDTO.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			MembersDTO membersDto = new MembersDTO();
			if (membersFile.length() != 0) {
				membersDto = (MembersDTO) jaxbUnmarshaller.unmarshal(membersFile);
			}
			
			for (MemberDTO memberDto : membersDto.getMembers()) {
				Member member = new Member(memberDto.getId(), memberDto.getFirstName(),
							memberDto.getLastName(), null, new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a")
							.parse(memberDto.getRegistrationDate()));
				
				if (memberDto.getMembershipId() != null) {
					jaxbContext = JAXBContext.newInstance(MembershipsDTO.class);
					jaxbUnmarshaller = jaxbContext.createUnmarshaller();
					MembershipsDTO membershipsDto = new MembershipsDTO();
					if (membershipsFile.length() != 0) {
						membershipsDto = (MembershipsDTO) jaxbUnmarshaller.unmarshal(membershipsFile);
					}
					
					for (MembershipDTO membershipDto : membershipsDto.getMemberships()) {
						if (membershipDto.getId() == memberDto.getMembershipId()) {
							List<Time> startTime = new ArrayList<Time>();
							for (String time : membershipDto.getStartTime().getTimes()) {
								if (time != null) { 
									startTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
								}
							}
							List<Time> endTime = new ArrayList<Time>();
							for (String time : membershipDto.getEndTime().getTimes()) {
								if (time != null) { 
									endTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(time).getTime()));
								}
							}
							member.setMembership(new Membership(membershipDto.getId(), membershipDto.getName(),
									membershipDto.getPrice(), startTime, endTime, null));
							break;
						}
					}
				}
				members.add(member);
			}
		} catch (JAXBException e) {
			log.error(e);
		} catch (ParseException e) {
			log.error(e);
		}
		return members;
	}
	
}
