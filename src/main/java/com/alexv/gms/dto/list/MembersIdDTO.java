package com.alexv.gms.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "membersId")
@XmlAccessorType(XmlAccessType.FIELD)
public class MembersIdDTO {
	
	@XmlElement(name = "id")
    private List<Long> ids;
	
	public MembersIdDTO() {
		ids = new ArrayList<Long>();
	}
 
    public List<Long> getIds() {
        return ids;
    }
 
    public void setIds(List<Long> ids) {
        this.ids = ids;
    }
	
}

