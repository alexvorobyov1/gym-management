package com.alexv.gms.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.factory.XMLDAOFactory;

@WebServlet("/delete-member")
public class MemberDeleteServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private DAOFactory daoFactory = new XMLDAOFactory();
	private MemberDAO memberDao = daoFactory.getMemberDao();
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String id = request.getParameter("id");
		memberDao.deleteById(Long.valueOf(id));
		response.sendRedirect(request.getContextPath() + "/members");
	}
	
}