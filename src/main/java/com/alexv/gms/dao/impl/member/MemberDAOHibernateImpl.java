package com.alexv.gms.dao.impl.member;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.domain.Member;

public class MemberDAOHibernateImpl implements MemberDAO {
	
	private static MemberDAOHibernateImpl instance;
	
	private static EntityManagerFactory emf;
	
	private MemberDAOHibernateImpl() {}
	
	public static MemberDAOHibernateImpl getInstance() {
		if (instance == null) {
			instance = new MemberDAOHibernateImpl();
			emf = Persistence.createEntityManagerFactory("gms");
		}
		return instance;
	}

	@Override
	public Member getById(Long id) {
		Member member = null;
		
	    EntityManager em = emf.createEntityManager();
	    em.getTransaction().begin();
	    
		member = em.find(Member.class, id);
		
		em.getTransaction().commit();
		em.close();
		
		return member;
	}

	@Override
	public void add(Member member) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.persist(member);
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void update(Member member) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.merge(member);
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void deleteById(Long id) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		em.remove(em.find(Member.class, id));
		
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public List<Member> getAll() {
		List<Member> members = new ArrayList<Member>();
		
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		members = em.createQuery("from Member", Member.class).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return members;
	}

}
