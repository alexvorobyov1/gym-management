package com.alexv.gms.dao.factory;

import java.io.File;

import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.impl.member.MemberDAOXMLImpl;
import com.alexv.gms.dao.impl.membership.MembershipDAOXMLImpl;

public class XMLDAOFactory implements DAOFactory {
	
	public static File getXmlFile(String fileName) {
		return new File(XMLDAOFactory.class.getClassLoader()
					.getResource(fileName).getPath()
					.replaceAll("%20", " "));
	}
	
	@Override
	public MemberDAO getMemberDao() {
		return MemberDAOXMLImpl.getInstance();
	}

	@Override
	public MembershipDAO getMembershipDao() {
		return MembershipDAOXMLImpl.getInstance();
	}
	
}
