package com.alexv.gms.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.MemberDAO;
import com.alexv.gms.dao.factory.CollectionDAOFactory;
import com.alexv.gms.domain.Member;
import com.alexv.gms.domain.Membership;

public class MemberDAOCollectionImplTest {
	
	private static DAOFactory daoFactory;
	private static MemberDAO memberDao;
	
	@BeforeClass
	public static void setUp() {
		daoFactory = new CollectionDAOFactory();
		memberDao = daoFactory.getMemberDao();
		memberDao.add(new Member(null, "foo1", "bar1", new Membership(), null));
		memberDao.add(new Member(null, "foo2", "bar2", new Membership(), null));
		memberDao.add(new Member(null, "foo3", "bar3", new Membership(), null));
		for (Member member : memberDao.getAll()) {
			member.setRegistrationDate(new Date(0));
		}
	}
	
	@Test
	public void testShouldGetMemberById() {
		Member expected = new Member(1L, "foo1", "bar1", new Membership(), new Date(0));
		
		Member actual = memberDao.getById(1L);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldGetAllMembers() {
		List<Member> expected = new ArrayList<Member>();
		expected.add(new Member(1L, "foo1", "bar1", new Membership(), new Date(0)));
		expected.add(new Member(2L, "foo2", "bar2", new Membership(), new Date(0)));
		expected.add(new Member(3L, "foo3", "bar3", new Membership(), new Date(0)));
		
		List<Member> actual = memberDao.getAll();
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldAddMember() {
		Member expected = new Member(4L, "foo4", "bar4", new Membership(), new Date(0));
		
		memberDao.add(new Member(null, "foo4", "bar4", new Membership(), null));
		memberDao.getById(4L).setRegistrationDate(new Date(0));
		Member actual = memberDao.getById(4L);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldUpdateMember() {
		Member expected = new Member(1L, "foo1new", "bar1new", new Membership(), new Date(0));
		
		Member member = new Member(1L, "foo1new", "bar1new", new Membership(), new Date(0));
		memberDao.update(member);
		Member actual = memberDao.getById(1L);
		
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testShouldDeleteMemberById() {
		memberDao.deleteById(1L);
		Member actual = memberDao.getById(1L);
		
		assertNull(actual);	
	}
	
}
