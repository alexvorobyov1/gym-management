CREATE TABLE `members` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`first_name` varchar(45) DEFAULT NULL,
	`last_name` varchar(45) DEFAULT NULL,
	`registration_date` timestamp NULL DEFAULT NULL,
	`membership_id` bigint(20) DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `membership_id_idx` (`membership_id`),
	CONSTRAINT `members_membership_id`
	FOREIGN KEY (`membership_id`)
	REFERENCES `memberships` (`id`)
	ON DELETE SET NULL
	ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `memberships` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`name` varchar(45) DEFAULT NULL,
	`price` double DEFAULT NULL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `start_time` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`time` time DEFAULT NULL,
	`membership_id` bigint(20) DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `membership_id_idx` (`membership_id`),
	CONSTRAINT `start_time_membership_id` 
	FOREIGN KEY (`membership_id`) 
	REFERENCES `memberships` (`id`) 
	ON DELETE CASCADE 
	ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8

CREATE TABLE `end_time` (
	`id` bigint(20) NOT NULL AUTO_INCREMENT,
	`time` time DEFAULT NULL,
	`membership_id` bigint(20) DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `membership_id_idx` (`membership_id`),
	CONSTRAINT `end_time_membership_id` 
	FOREIGN KEY (`membership_id`) 
	REFERENCES `memberships` (`id`) 
	ON DELETE CASCADE 
	ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8