package com.alexv.gms.dao;

import java.util.List;

import com.alexv.gms.domain.Membership;

public interface MembershipDAO {
	
	Membership getById(Long id);
	
	void add(Membership membership);
	
	void update(Membership membership);
	
	void deleteById(Long id);
	
	List<Membership> getAll();
	
}
