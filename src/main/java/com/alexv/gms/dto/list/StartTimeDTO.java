package com.alexv.gms.dto.list;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "startTime")
@XmlAccessorType(XmlAccessType.FIELD)
public class StartTimeDTO {

	@XmlElement(name = "time")
    private List<String> times;
	
	public StartTimeDTO() {
		times = new ArrayList<String>();
	}
 
    public List<String> getTimes() {
        return times;
    }
 
    public void setTimes(List<String> times) {
        this.times = times;
    }
	
}
