package com.alexv.gms.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.factory.CollectionDAOFactory;
import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.domain.Membership;

public class MembershipDAOCollectionImplTest {

	private static DAOFactory daoFactory;
	private static MembershipDAO membershipDao;
	
	@BeforeClass
	public static void setUp() {
		daoFactory = new CollectionDAOFactory();
		membershipDao = daoFactory.getMembershipDao();
	}
	
	@Test
	public void testShouldGetMembershipById() {
		Membership expected = new Membership(1L, "Bronze", 99.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("06:30:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("16:00:00"))), null);
		
		Membership actual = membershipDao.getById(1L);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldGetAllMemberships() {
		List<Membership> expected = new ArrayList<Membership>();
		expected.add(new Membership(1L, "Bronze", 99.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("06:30:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("16:00:00"))), null));
		expected.add(new Membership(2L, "Silver", 199.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("06:30:00"), Time.valueOf("20:00:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("16:00:00"), Time.valueOf("22:30:00"))), null));
		expected.add(new Membership(3L, "Gold", 299.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("06:30:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("22:30:00"))), null));
		
		List<Membership> actual = membershipDao.getAll();
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldAddMembership() {
		Membership expected = new Membership(4L, "foo", 500.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("01:00:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("02:00:00"))), null);
		
		membershipDao.add(new Membership(null, "foo", 500.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("01:00:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("02:00:00"))), null));
		Membership actual = membershipDao.getById(4L);
		
		assertEquals(expected, actual);		
	}
	
	@Test
	public void testShouldUpdateMembership() {
		Membership expected = new Membership(4L, "foonew", 1500.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("11:11:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("22:22:00"))), null);
		
		Membership membership = new Membership(4L, "foonew", 1500.0, new ArrayList<Time>(
				Arrays.asList(Time.valueOf("11:11:00"))), new ArrayList<Time>(
				Arrays.asList(Time.valueOf("22:22:00"))), null);
		membershipDao.update(membership);
		Membership actual = membershipDao.getById(4L);
		
		assertEquals(expected, actual);	
	}
	
	@Test
	public void testShouldDeleteMembershipById() {
		membershipDao.deleteById(1L);
		Membership actual = membershipDao.getById(1L);
		
		assertNull(actual);	
	}
	
}
