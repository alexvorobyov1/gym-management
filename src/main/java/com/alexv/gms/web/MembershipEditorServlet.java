package com.alexv.gms.web;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.alexv.gms.dao.MembershipDAO;
import com.alexv.gms.dao.factory.DAOFactory;
import com.alexv.gms.dao.factory.XMLDAOFactory;
import com.alexv.gms.domain.Membership;

@WebServlet("/membership")
public class MembershipEditorServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = Logger.getLogger(MembershipEditorServlet.class);

	private DAOFactory daoFactory = new XMLDAOFactory();
	private MembershipDAO membershipDao = daoFactory.getMembershipDao();
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		int timeListSize = request.getParameter("timeListSize") == null ? 1 : 
			Integer.valueOf(request.getParameter("timeListSize"));
		if (id != null && !id.isEmpty()) {
			Membership membership = membershipDao.getById(Long.valueOf(id));
			request.setAttribute("membership", membership);
			int biggerListSize = membership.getStartTime().size() >= membership.getEndTime().size() ?
					membership.getStartTime().size() : membership.getEndTime().size();
			int sizeGreaterThanZero = biggerListSize > 0  ? biggerListSize : 1; 
			timeListSize = request.getParameter("timeListSize") == null ? sizeGreaterThanZero : 
				Integer.valueOf(request.getParameter("timeListSize"));
		}
		request.setAttribute("timeListSize", timeListSize);

		getServletContext().getRequestDispatcher("/WEB-INF/pages/membership-form.jsp").forward(request, response);
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String price = request.getParameter("price");
		int timeListSize = Integer.valueOf(request.getParameter("timeListSize"));
		Membership membership = new Membership();
		membership.setName(request.getParameter("name"));
		membership.setPrice(price == null || price.isEmpty() ? 0 : Double.valueOf(price));
		List<Time> startTime = new ArrayList<Time>();
		List<Time> endTime = new ArrayList<Time>();
		try {
			for (int i = 0; i < timeListSize; i++) {
				String currTime = request.getParameter("startTime" + i);
				if (currTime != null && !currTime.isEmpty()) {	
					startTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(currTime).getTime()));
				} else {
					startTime.add(null);
				}
			}
			for (int i = 0; i < timeListSize; i++) {	
				String currTime = request.getParameter("endTime" + i);
				if (currTime != null && !currTime.isEmpty()) {
					endTime.add(new Time(new SimpleDateFormat("hh:mm a").parse(currTime).getTime()));
				} else {
					endTime.add(null);
				}
			}
		} catch (ParseException e) {
			log.error(e);
		}	
		membership.setStartTime(startTime);
		membership.setEndTime(endTime);
		if (id == null || id.isEmpty()) {
			membershipDao.add(membership);
		} else {
			membership.setId(Long.valueOf(id));
			membershipDao.update(membership);
		}
		
		response.sendRedirect(request.getContextPath() + "/memberships");
	}
	
}
