INSERT INTO memberships (name, price) VALUES ('Bronze', 99.0), ('Silver', 199.0), ('Gold', 299.0);
INSERT INTO start_time (time, membership_id) VALUES ('06:30:00', 1), ('06:30:00', 2), ('20:00:00', 2), ('06:30:00', 3);
INSERT INTO end_time (time, membership_id) VALUES ('16:00:00', 1), ('16:00:00', 2), ('22:30:00', 2), ('22:30:00', 3);